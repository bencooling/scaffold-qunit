'use strict';

var gulp = require('gulp')
  , qunit = require('gulp-qunit')
  ;

gulp.task('default', function() {
  return gulp.src('tests/test-runner.html')
    .pipe(qunit());
});