"use strict";

function foo() {
  $('#foo').text('foo');
}

module('DOM Example', {
  setup: function () {
    $('body').append('<div id="foo"></div>');
  },
  teardown: function () {
    $('#foo').remove();
  }
});
 
test('foo', function () {
  foo();
  equal($('#foo').text(), 'foo', "Foo has text foo!");
});